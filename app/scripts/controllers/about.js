'use strict';

/**
 * @ngdoc function
 * @name amuiwebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the amuiwebApp
 */
angular.module('amuiwebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
