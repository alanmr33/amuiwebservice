'use strict';

/**
 * @ngdoc function
 * @name amuiwebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the amuiwebApp
 */
angular.module('amuiwebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
